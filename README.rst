

=================================================================
numdiff: a package for easily switching Python numerical backends
=================================================================



License
=======



This software is published under the `GPLv3 license <https://www.gnu.org/licenses/gpl-3.0.en.html>`_.


