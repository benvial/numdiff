

.. image:: https://img.shields.io/gitlab/pipeline/benvial/numdiff/docker/main?style=for-the-badge
   :target: https://gitlab.com/benvial/numdiff/docker/commits/main
   :alt: pipeline status

.. image:: https://img.shields.io/docker/v/benvial/numdiff?color=8678bd&label=dockerhub&logo=docker&logoColor=white&style=for-the-badge
  :target: https://hub.docker.com/repository/docker/benvial/numdiff
  :alt: dockerhub

.. image:: https://img.shields.io/docker/pulls/benvial/numdiff?color=8678bd&style=for-the-badge
  :alt: docker pulls

.. image:: https://img.shields.io/docker/image-size/benvial/numdiff?color=8678bd&style=for-the-badge
  :alt: docker image size
  
.. image:: https://img.shields.io/badge/license-GPLv3-blue?color=bb798f&style=for-the-badge
  :target: https://gitlab.com/benvial/numdiff/docker/-/blob/main/LICENCE.txt
  :alt: license



Docker for numdiff
==================

Container based on miniconda with install numerical backends (numpy, torch, jax, autograd).

