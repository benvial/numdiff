
PROJECT_NAME := "numdiff"

BRANCH := "$(git branch --show-current)"

PROJECT_DIR := "$(realpath $PWD)"

VERSION := """$(python3 -c "from configparser import ConfigParser; p = ConfigParser(); p.read('setup.cfg'); print(p['metadata']['version'])")"""


GITLAB_PROJECT_ID := "40881481"


# List recipes
list:
    just -l


# Echo information
info:
    @echo {{PROJECT_NAME}} version {{VERSION}}, on branch {{BRANCH}}
    @echo directory {{PROJECT_DIR}}


# Make conda environment
conda-env:
    mamba env create -f environment.yml

# Install the python package locally in editable mode
install:
    pip install -e .

# Cleanup
clean:
    rm -rf build dist


# Push to gitlab
gl:
    @git add -A
    @read -p "Enter commit message: " MSG; \
    git commit -a -m "$MSG"
    @git push origin {{BRANCH}}


# Show gitlab repository
repo:
	xdg-open https://gitlab.com/benvial/{{PROJECT_NAME}}


# Clean, reformat and push to gitlab
save: gl


# Tag and push tags
tag: clean 
	@if [ "$(git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@echo "Version v{{VERSION}}"
	# @git add -A
	# git commit -a -m "Publish v{{VERSION}}"
	# @git push origin {{BRANCH}}
	@git tag v{{VERSION}} || echo Ignoring tag since it already exists
	@git push --tags || echo Ignoring tag since it already exists on the remote

# Create a release
release:
	@if [ "$(git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@gitlab project-release create --project-id {{GITLAB_PROJECT_ID}} \
	--name "version {{VERSION}}" --tag-name "v{{VERSION}}" --description "Released version {{VERSION}}"


# Create python package
package:
	@if [ "$(git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@rm -f dist/*
	@python3 -m build --sdist --wheel .

# Upload to pypi
pypi: package
	@twine upload dist/*


